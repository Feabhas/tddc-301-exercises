#include "unity.h"
#include "temperature_sensor.h"


// include dependent modules for compilation/linking
#include "mock_ds18b20.h"
#include "mock_lcd.h"

#include <string.h>

void setUp(void)
{
}

void tearDown(void)
{
}

void test_temperature_sensor_initialization_code(void)
{
    // CMock
    // ds18b20_initialise_ExpectAndReturn(R12, true);
    // lcd_initialize_ExpectAndReturn(true);

    // fff
    ds18b20_initialise_fake.return_val = true;
    lcd_initialize_fake.return_val = true;

    ds18b20_ROM_t data = { 
        .rom_code.family_code = 0x28, 
        .rom_code.serial_number = { 0x1, 0x2, 0x3, 0x4, 0x5, 0x6 } ,
        .rom_code.crc = 0x00, 
    };

    // ds18b20_read_rom_ExpectAndReturn(data);
    ds18b20_read_rom_fake.return_val = data;

    // ds18b20_calculate_CRC_ExpectAndReturn((void*)&data, sizeof(data)-1, data.rom_code.crc);
    ds18b20_calculate_CRC_fake.return_val = data.rom_code.crc;

    char fc_str[] = "Family code: 0x28";
    // lcd_display_ExpectAndReturn(fc_str, strlen(fc_str)-1);
    lcd_display_fake.return_val = strlen(fc_str)-1;

    char sn_str[] = "Serial Number: 06:05:04:03:02:01";
    // lcd_display_ExpectAndReturn(sn_str, strlen(sn_str)-1);
    lcd_display_fake.return_val = strlen(sn_str)-1;

    // Call UUT
    temperture_sensor_initialize();
    TEST_ASSERT_CALLED_TIMES(2, lcd_display);

    char msg[40] = "Family code: 0x28";
    lcd_display(msg);

    // TEST_ASSERT_EQUAL(1, ds18b20_initialise_fake.call_count);
    TEST_ASSERT_CALLED(ds18b20_initialise);
    TEST_ASSERT_EQUAL(R12, ds18b20_initialise_fake.arg0_val);

    TEST_ASSERT_CALLED(lcd_initialize);

    TEST_ASSERT_CALLED(ds18b20_read_rom);
    TEST_ASSERT_CALLED(ds18b20_calculate_CRC);

    TEST_ASSERT_CALLED_TIMES(3, lcd_display);
    // TEST_ASSERT_EQUAL_STRING("Family code: 0x28",lcd_display_fake.arg0_history[0]);
    TEST_ASSERT_EQUAL_STRING("Family code: 0x28",lcd_display_fake.arg0_history[2]);

}


void test_temperature_sensor_main_code(void)
{
    // ds18b20_do_conversion_Expect();

    // bool ds18b20_read_scratchpad(ds18b20_scratchpad_data_t * const data);
    ds18b20_scratchpad_data_t scratchpad = {
        .lsb = 0x50,
        .msb = 0x05,
        .crc = 0
    };
    // ds18b20_read_scratchpad_ExpectAndReturn(0, true);
    // ds18b20_read_scratchpad_IgnoreArg_data();
    // ds18b20_read_scratchpad_ReturnThruPtr_data(&scratchpad);


    ds18b20_read_scratchpad_fake.return_val = true;

    // ds18b20_calculate_CRC_ExpectAndReturn((void*)&scratchpad, sizeof(scratchpad)-1, scratchpad.crc);

    // ds18b20_convert_ExpectAndReturn(0x0550U, 85.0f);

    // lcd_display_ExpectAndReturn("85.00C", 6);

    // run UUT
    temperture_sensor_run();

    TEST_ASSERT_CALLED(ds18b20_read_scratchpad);

}


// ///// INITIALIZATION FAILURE TESTS /////
// void test_temperature_sensor_initialization_ds18b20_failure(void)
// {
//     // ds18b20_initialise_ExpectAndReturn(R12, false);
//     // Call UUT
//     temperture_sensor_initialize();
// }

// void test_temperature_sensor_initialization_lcd_failure(void)
// {
//     // ds18b20_initialise_ExpectAndReturn(R12, true);
//     // lcd_initialize_ExpectAndReturn(false);

//     // Call UUT
//     temperture_sensor_initialize();
// }

// void test_temperature_sensor_initialization_crc_fail(void)
// {
//     // ds18b20_initialise_ExpectAndReturn(R12, true);
//     // lcd_initialize_ExpectAndReturn(true);

//     ds18b20_ROM_t data = { 
//         .rom_code.family_code = 0x28, 
//         .rom_code.serial_number = { 0x1, 0x2, 0x3, 0x4, 0x5, 0x6 } ,
//         .rom_code.crc = 0x00, 
//     };

//     // ds18b20_read_rom_ExpectAndReturn(data);

//     // ds18b20_calculate_CRC_ExpectAndReturn((void*)&data, sizeof(data)-1, 0xFF);

//     // Call UUT
//     temperture_sensor_initialize();
// }

// void test_temperature_sensor_initialization_lcd_write_fail_family(void)
// {
//     ds18b20_initialise_ExpectAndReturn(R12, true);
//     lcd_initialize_ExpectAndReturn(true);

//     ds18b20_ROM_t data = { 
//         .rom_code.family_code = 0x28, 
//         .rom_code.serial_number = { 0x1, 0x2, 0x3, 0x4, 0x5, 0x6 } ,
//         .rom_code.crc = 0x00, 
//     };

//     ds18b20_read_rom_ExpectAndReturn(data);

//     ds18b20_calculate_CRC_ExpectAndReturn((void*)&data, sizeof(data)-1, data.rom_code.crc);

//     const char* fc_str = "Family code: 0x28";
//     lcd_display_ExpectAndReturn(fc_str, -1);

//     // Call UUT
//     temperture_sensor_initialize();
// }

// void test_temperature_sensor_initialization_lcd_write_fail_sn(void)
// {
//     ds18b20_initialise_ExpectAndReturn(R12, true);
//     lcd_initialize_ExpectAndReturn(true);

//     ds18b20_ROM_t data = { 
//         .rom_code.family_code = 0x28, 
//         .rom_code.serial_number = { 0x1, 0x2, 0x3, 0x4, 0x5, 0x6 } ,
//         .rom_code.crc = 0x00, 
//     };

//     ds18b20_read_rom_ExpectAndReturn(data);

//     ds18b20_calculate_CRC_ExpectAndReturn((void*)&data, sizeof(data)-1, data.rom_code.crc);

//     const char* fc_str = "Family code: 0x28";
//     lcd_display_ExpectAndReturn(fc_str, strlen(fc_str)-1);
//     const char* sn_str = "Serial Number: 06:05:04:03:02:01";
//     lcd_display_ExpectAndReturn(sn_str, -1);

//     // Call UUT
//     temperture_sensor_initialize();
// }

// ///// MAIN FAILURE TESTS /////
// void test_temperature_sensor_main_code_read_scratchpad_error(void)
// {
//     ds18b20_do_conversion_Expect();

//     // bool ds18b20_read_scratchpad(ds18b20_scratchpad_data_t * const data);
//     ds18b20_scratchpad_data_t scratchpad = {
//         .lsb = 0x50,
//         .msb = 0x05,
//         .crc = 0
//     };
//     ds18b20_read_scratchpad_ExpectAndReturn(0, false);
//     ds18b20_read_scratchpad_IgnoreArg_data();

//     // run UUT
//     temperture_sensor_run();
// }

// void test_temperature_sensor_main_code_crc_error(void)
// {
//     ds18b20_do_conversion_Expect();

//     // bool ds18b20_read_scratchpad(ds18b20_scratchpad_data_t * const data);
//     ds18b20_scratchpad_data_t scratchpad = {
//         .lsb = 0x50,
//         .msb = 0x05,
//         .crc = 0
//     };
//     ds18b20_read_scratchpad_ExpectAndReturn(0, true);
//     ds18b20_read_scratchpad_IgnoreArg_data();
//     ds18b20_read_scratchpad_ReturnThruPtr_data(&scratchpad);

//     ds18b20_calculate_CRC_ExpectAndReturn((void*)&scratchpad, sizeof(scratchpad)-1, 0xFF);

//     // run UUT
//     temperture_sensor_run();
// }

// void test_temperature_sensor_main_code_converts_fail_neg(void)
// {
//     ds18b20_do_conversion_Expect();

//     // bool ds18b20_read_scratchpad(ds18b20_scratchpad_data_t * const data);
//     ds18b20_scratchpad_data_t scratchpad = {
//         .lsb = 0x50,
//         .msb = 0x05,
//         .crc = 0
//     };
//     ds18b20_read_scratchpad_ExpectAndReturn(0, true);
//     ds18b20_read_scratchpad_IgnoreArg_data();
//     ds18b20_read_scratchpad_ReturnThruPtr_data(&scratchpad);

//     ds18b20_calculate_CRC_ExpectAndReturn((void*)&scratchpad, sizeof(scratchpad)-1, scratchpad.crc);

//     ds18b20_convert_ExpectAndReturn(0x0550U, -100.0f);

//     // run UUT
//     temperture_sensor_run();
// }

// void test_temperature_sensor_main_code_converts_fail_positive(void)
// {
//     ds18b20_do_conversion_Expect();

//     // bool ds18b20_read_scratchpad(ds18b20_scratchpad_data_t * const data);
//     ds18b20_scratchpad_data_t scratchpad = {
//         .lsb = 0x50,
//         .msb = 0x05,
//         .crc = 0
//     };
//     ds18b20_read_scratchpad_ExpectAndReturn(0, true);
//     ds18b20_read_scratchpad_IgnoreArg_data();
//     ds18b20_read_scratchpad_ReturnThruPtr_data(&scratchpad);

//     ds18b20_calculate_CRC_ExpectAndReturn((void*)&scratchpad, sizeof(scratchpad)-1, scratchpad.crc);

//     ds18b20_convert_ExpectAndReturn(0x0550U, 126.0f);

//     // run UUT
//     temperture_sensor_run();
// }

// void test_temperature_sensor_main_code_lcd_write_fail(void)
// {
//     ds18b20_do_conversion_Expect();

//     // bool ds18b20_read_scratchpad(ds18b20_scratchpad_data_t * const data);
//     ds18b20_scratchpad_data_t scratchpad = {
//         .lsb = 0x50,
//         .msb = 0x05,
//         .crc = 0
//     };
//     ds18b20_read_scratchpad_ExpectAndReturn(0, true);
//     ds18b20_read_scratchpad_IgnoreArg_data();
//     ds18b20_read_scratchpad_ReturnThruPtr_data(&scratchpad);

//     ds18b20_calculate_CRC_ExpectAndReturn((void*)&scratchpad, sizeof(scratchpad)-1, scratchpad.crc);

//     ds18b20_convert_ExpectAndReturn(0x0550U, 85.0f);

//     lcd_display_ExpectAndReturn("85.00C", -1);

//     // run UUT
//     temperture_sensor_run();
// }
