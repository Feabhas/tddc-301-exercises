#include "temperature_sensor.h"
#include "ds18b20.h"
#include "lcd.h"

#include "unity.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

// a.  Initialize the DS18B20 to be configurable for 9, 10, 11 or 12-bit resolution
// b.  Read ROM
// c.  Check ROM CRC value
// d.  On a LCD display print out ROM's Family Code and Serial number values
void temperture_sensor_initialize(void)
{
    bool okay = false;
    okay = ds18b20_initialise(R12);
    if(!okay){
        // TODO : work out how to fail?
        // should we have a return code?
        return;
    }

    okay = lcd_initialize();
    if(!okay){
        // TODO : work out how to fail?
        // should we have a return code?
        return;
    }

    ds18b20_ROM_t rom_data = ds18b20_read_rom();
    uint8_t crc = ds18b20_calculate_CRC((uint8_t*)&rom_data, sizeof(ds18b20_ROM_t)-1);
    if(rom_data.rom_code.crc != crc){
        // TODO : work out how to fail?
        // should we have a return code?
        return;
    }

    char buff[40] = { '\0' };
    sprintf(buff, "Family code: 0x%02X", rom_data.rom_code.family_code);
    int count = 0;
    count = lcd_display(buff);
    if(count == -1) {
        // LCD display error
        return;
    }

    memset(buff, 0, sizeof(buff));
    int num = sprintf(buff, "Serial Number: ");
    for (uint32_t i = 6, offset = 0; i != 0; --i) {
        int xnum = sprintf((buff+(num+offset)), "%02X%s", rom_data.rom_code.serial_number[i-1], (i != 1)?":":"");
        offset += xnum;
    }
    count = lcd_display(buff);
    if(count == -1) {
        // LCD display error
        return;
    }
}


// a.  Instruct the DS18B20 to do a conversion 
// b.  Read the Scratchpad
// c.  Check Scratchpad data CRC
// d.  Convert 16-bit raw temperature to floating point degree C
// e.  Convert float to C-string format <nn.nnC>
// f.  Call LCD display to print C-string value
void temperture_sensor_run(void)
{
    ds18b20_do_conversion();
    ds18b20_scratchpad_data_t scratchpad;
    memset(&scratchpad, 0, sizeof(scratchpad));

    bool okay = ds18b20_read_scratchpad(&scratchpad);
    if(!okay){
        // TODO : work out how to fail?
        // should we have a return code?
        return;
    }

    uint8_t crc = ds18b20_calculate_CRC((uint8_t*)&scratchpad, sizeof(scratchpad)-1);
    if(scratchpad.crc != crc){
        // TODO : work out how to fail?
        // simple is not to update the display
        // we could try converting again and have a max number of attempts?
        return;
    }

    uint16_t raw_sensor_temperature = ((scratchpad.msb << 8) | scratchpad.lsb);

    float deg_C = ds18b20_convert(raw_sensor_temperature);
    if(deg_C < -55.0f) {
        // below min Out of range error
        return;
    }
    else if(deg_C > 125.0f) {
        // Above max Out of range error
        return;
    }

    char buff[20] = { '\0' };
    sprintf(buff, "%02.2fC", deg_C);

    int count = lcd_display(buff);
    if(count == -1) {
        // LCD display error
        return;
    }
}