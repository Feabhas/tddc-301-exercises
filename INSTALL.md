# install ruby

```
$ sudo apt install ruby-full
$ ruby -v
```

# install ceedling
 
see http://www.throwtheswitch.org/ceedling

```
$ sudo gem install ceedling
```

# install valgrind

```
sudo apt-get install -y valgrind
```