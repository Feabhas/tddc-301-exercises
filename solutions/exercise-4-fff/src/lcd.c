#include "lcd.h"
#include <string.h>

#ifndef MOCKING

bool lcd_initialize(void)
{
    return true;
}

int lcd_display(const char* str)
{
    return strlen(str)-1;
}

#endif
